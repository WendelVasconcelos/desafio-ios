//
//  PrevisoesTableViewController.swift
//  desafio-ios
//
//  Created by Wendel Vasconcelos on 13/03/17.
//  Copyright © 2017 opovo. All rights reserved.
//

import UIKit
import CoreLocation



class PrevisoesTableViewController: UITableViewController{
    
    var previsoes = NSArray()
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    var activityIndicator = UIActivityIndicatorView()
        override func viewDidLoad() {
        super.viewDidLoad()
            if let lat = UserDefaults.standard.object(forKey: "latitude") as? CLLocationDegrees{
                latitude = lat
            }
            if let long = UserDefaults.standard.object(forKey: "longitude") as? CLLocationDegrees{
                longitude = long
            }
            
            
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
            
            do{
                let clientURL = NSURL(string: "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(latitude!)&lon=\(longitude!)&APPID=3052b65b4cf96fedb1690728919bf1d6&cnt=15")!
                let clientRequest = NSMutableURLRequest(url: clientURL as URL)
                clientRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                clientRequest.httpMethod = "POST"
                
                URLSession.shared.dataTask(with: clientRequest as URLRequest) { (data, response, error) -> Void in
                    if error != nil{
                        print("error: \(error)")
                        
                    }else{
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                           if let urlContent = data {
                            do {
                                let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: .allowFragments) as! NSDictionary
                                //print(jsonResult)
                                let city = jsonResult["city"] as! NSDictionary
                                let name = city["name"] as! String
                                self.title = name
                                self.previsoes = jsonResult["list"] as! NSArray
                                
                                
                                
                                self.tableView.reloadData()
                                
                            } catch {
                                
                                print("JSON Processing Failed")
                                
                            }
                            }
                            
                        }
                        
                    }
                    }.resume()
                
            }catch{
                
            }
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return previsoes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TempoTableViewCell
        let lista = self.previsoes[indexPath.row] as! NSDictionary
        let temp = lista["temp"] as! NSDictionary
        let min = temp["min"] as! Double - 273.15
        let max = temp["max"] as! Double - 273.15
        cell.variacao.text = "Máx: \(max) °C / Min: \(min) °C"
        let date = Date()
        let cal = Calendar.current
        let day = cal.component(.day, from: date)
        cell.data.text = "\(day + indexPath.row) de Março"
    
        
        let condicoes = lista["weather"] as! NSArray
        let condicao_principal = (condicoes[0] as! NSDictionary)["main"] as! String
        if condicao_principal == "Rain"{
            cell.condicao.image = UIImage(named: "Rain")
        }else if condicao_principal == "Sun"{
            cell.condicao.image = UIImage(named: "Sun")
        }else{
            cell.condicao.image = UIImage(named: "Partly Cloudy")
        }
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
