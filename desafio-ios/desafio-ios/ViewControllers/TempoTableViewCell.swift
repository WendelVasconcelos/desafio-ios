//
//  TempoTableViewCell.swift
//  desafio-ios
//
//  Created by Wendel Vasconcelos on 13/03/17.
//  Copyright © 2017 opovo. All rights reserved.
//

import UIKit

class TempoTableViewCell: UITableViewCell {

    @IBOutlet weak var data: UILabel!
    @IBOutlet weak var condicao: UIImageView!
    
    @IBOutlet weak var variacao: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
